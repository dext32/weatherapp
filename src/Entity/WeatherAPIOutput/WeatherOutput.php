<?php

namespace App\Entity\WeatherAPIOutput;

class WeatherOutput implements WeatherAPIOutputInterface
{

    public function query($inputQuery)
    {
        return "weather?q=".$inputQuery;
    }

}
