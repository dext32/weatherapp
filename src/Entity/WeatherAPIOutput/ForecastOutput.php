<?php

namespace App\Entity\WeatherAPIOutput;

class ForecastOutput implements WeatherAPIOutputInterface
{

    public function query($inputQuery)
    {
        return "forecast?q=".$inputQuery;
    }

}