<?php

namespace App\Entity\WeatherAPIOutput;

interface WeatherAPIOutputInterface
{

    public function query($inputQuery);

}

