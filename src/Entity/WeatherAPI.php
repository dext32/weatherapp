<?php

namespace App\Entity;

use App\Entity\WeatherAPIOutput;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeatherAPIRepository")
 */
class WeatherAPI
{
    /**
     * @var string
     */
    private static $apiKey = 'e77f09e3196bfa9603bf00650a5ae7e7';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="object")
     */
    private $apiResponse;

    /**
     * @var WeatherAPIOutput\WeatherAPIOutputInterface
     */
    private $apiOutput;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApiResponse()
    {
        return $this->apiResponse;
    }

    public function setApiResponse($apiResponse): self
    {
        $this->apiResponse = $apiResponse;

        return $this;
    }

    public function setApiOutput(WeatherAPIOutput\WeatherAPIOutputInterface $apiOutput)
    {
        $this->apiOutput = $apiOutput;

        return $this;
    }

    public function queryApi($inputQuery)
    {
        return $this->curlQueryByString($this->apiOutput->query($inputQuery));
    }


    public function curlQueryByString($apiRequest)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'api.openweathermap.org/data/2.5/'.$apiRequest.'&appid='.self::$apiKey);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curl);

        return json_decode($response);
    }

    public function setOpenWeatherMapResponse()
    {
        $apiResponse = $this->getApiResponse();

        $owm = new OpenWeatherMapResponse();
        $output_owm = $owm
            ->setId($apiResponse->id)
            ->setBase($apiResponse->base)
            ->setClouds($apiResponse->clouds)
            ->setCod($apiResponse->cod)
            ->setCoord($apiResponse->coord)
            ->setDt($apiResponse->dt)
            ->setMain($apiResponse->main)
            ->setName($apiResponse->name)
            //->setRain($apiResponse->rain)
            ->setSys($apiResponse->sys)
            ->setWeather($apiResponse->weather)
            ->setWind($apiResponse->wind)
            ->setLastUpdated((new \DateTime())->format('Y-m-d H:i:s'));

        return $output_owm;
    }


}
