<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\OpenWeatherMapResponseRepository")
 */
class OpenWeatherMapResponse
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="object")
     */
    private $coord;

    /**
     * @ORM\Column(type="array")
     */
    private $weather = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $base;

    /**
     * @ORM\Column(type="object")
     */
    private $main;

    /**
     * @ORM\Column(type="object")
     */
    private $wind;

    /**
     * @ORM\Column(type="object")
     */
    private $rain;

    /**
     * @ORM\Column(type="object")
     */
    private $clouds;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dt;

    /**
     * @ORM\Column(type="object")
     */
    private $sys;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cod;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCoord()
    {
        return $this->coord;
    }

    public function setCoord($coord): self
    {
        $this->coord = $coord;

        return $this;
    }

    public function getWeather(): ?array
    {
        return $this->weather;
    }

    public function setWeather(array $weather): self
    {
        $this->weather = $weather;

        return $this;
    }

    public function getBase(): ?string
    {
        return $this->base;
    }

    public function setBase(string $base): self
    {
        $this->base = $base;

        return $this;
    }

    public function getMain()
    {
        return $this->main;
    }

    public function setMain($main): self
    {
        $this->main = $main;

        return $this;
    }

    public function getWind()
    {
        return $this->wind;
    }

    public function setWind($wind): self
    {
        $this->wind = $wind;

        return $this;
    }

    public function getRain()
    {
        return $this->rain;
    }

    public function setRain($rain): self
    {
        $this->rain = $rain;

        return $this;
    }

    public function getClouds()
    {
        return $this->clouds;
    }

    public function setClouds($clouds): self
    {
        $this->clouds = $clouds;

        return $this;
    }

    public function getDt(): ?string
    {
        return $this->dt;
    }

    public function setDt(string $dt): self
    {
        $this->dt = $dt;

        return $this;
    }

    public function getSys()
    {
        return $this->sys;
    }

    public function setSys($sys): self
    {
        $this->sys = $sys;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCod(): ?string
    {
        return $this->cod;
    }

    public function setCod(string $cod): self
    {
        $this->cod = $cod;

        return $this;
    }

    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    public function setLastUpdated($last_updated): self
    {
        $this->last_updated = $last_updated;

        return $this;
    }

}
