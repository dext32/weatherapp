<?php

namespace App\Repository;

use App\Entity\WeatherAPI;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WeatherAPI|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherAPI|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherAPI[]    findAll()
 * @method WeatherAPI[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherAPIRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WeatherAPI::class);
    }

    public function findByWeatherDataArray(array $value)
    {
        return $this->findOneBy($value);
    }


    // /**
    //  * @return WeatherAPI[] Returns an array of WeatherAPI objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WeatherAPI
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
