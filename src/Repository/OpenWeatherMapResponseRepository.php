<?php

namespace App\Repository;

use App\Entity\OpenWeatherMapResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OpenWeatherMapResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenWeatherMapResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenWeatherMapResponse[]    findAll()
 * @method OpenWeatherMapResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenWeatherMapResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OpenWeatherMapResponse::class);
    }


    /**
     * @param $name
     * @return OpenWeatherMapResponse[]
     */
    public function findAllNamedOrderedByLastUpdate($name){
        return $this->createQueryBuilder('weather')
            ->andWhere('weather.name = :weatherName')
            ->setParameter('weatherName',true)
            ->addOrderBy('weather.last_updated','DESC')
            ->getQuery()
            ->execute();
    }

    // /**
    //  * @return OpenWeatherMapResponse[] Returns an array of OpenWeatherMapResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OpenWeatherMapResponse
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
