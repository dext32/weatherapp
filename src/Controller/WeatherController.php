<?php

namespace App\Controller;

use App\Entity\OpenWeatherMapResponse;
use App\Entity\WeatherAPI;
use App\Entity\WeatherAPIOutput;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class WeatherController extends AbstractController
{
    /**
     * @Route("/weather/{cityName}", name="weather")
     */
    public function index($cityName)
    {
        $em = $this->getDoctrine()->getManager();
        $weatherApi = new WeatherAPI();
        $weather_output = new WeatherAPIOutput\WeatherOutput();

        $weatherApi -> setApiOutput($weather_output);

        $data = $weatherApi->queryApi($cityName);

        $weatherApi -> setApiResponse($data);
        $owm_output = $weatherApi->setOpenWeatherMapResponse();

        $last_owm = $em->getRepository(OpenWeatherMapResponse::class)
            ->findAllNamedOrderedByLastUpdate($cityName);

        echo "<pre>";
        print_r($last_owm);
        echo "</pre>";

        $em -> persist($owm_output);
        $em -> flush();

        echo "<pre>";
            print_r($data);
        echo "</pre>";

        return $this->render('weather/index.html.twig', [
            'controller_name' => 'WeatherController',
            'weatherData' => $data,
        ]);
    }
}
